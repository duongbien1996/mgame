import { Module } from '@nestjs/common';
import { GraphQLModule } from "@nestjs/graphql";
import { DatabaseModule } from "src/database/database.module";
import configuration from './config/configuration';
import { ConfigModule } from '@nestjs/config';
import { gqlOptions } from './graphql/gql-options';
import { CategoryModule } from "src/modules/categories/category.module";

const imports = [
  ConfigModule.forRoot({
      envFilePath: [
          ".env",
          ".env.develop",
          ".env.test",
          ".env.stag",
          ".env.prod",
      ],
      isGlobal: true,
      load: [configuration],
      expandVariables: true,
  }),
  DatabaseModule,
  GraphQLModule.forRoot(gqlOptions),
  CategoryModule
];
@Module({
  imports,
})
export class AppModule {}
