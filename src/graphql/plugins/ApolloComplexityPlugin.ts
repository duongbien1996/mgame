import { ApolloServerPlugin } from 'apollo-server-plugin-base'; // Sử dụng phiên bản mới nhất của Apollo Server Plugin
import { GraphQLSchema } from 'graphql';
import {
  fieldExtensionsEstimator,
  getComplexity,
  simpleEstimator,
} from 'graphql-query-complexity';
import { GraphQLError } from 'graphql';
import { Plugin } from '@nestjs/apollo';

@Plugin()
export class ApolloComplexityPlugin implements ApolloServerPlugin {
  private schema: GraphQLSchema;

  constructor(private readonly maxComplexity: number = 500) {}

  async serverWillStart() {
    // Đoạn mã này không cần thay đổi
  }

  async requestDidStart() {
    return {
      didResolveOperation: async ({ request, document }) => {
        const complexity = getComplexity({
          schema: this.schema,
          operationName: request.operationName,
          query: document,
          variables: request.variables,
          estimators: [
            fieldExtensionsEstimator(),
            simpleEstimator({ defaultComplexity: 1 }),
          ],
        });
        if (complexity > this.maxComplexity) {
          throw new GraphQLError(
            `Sorry, too complicated query! ${complexity} is over ${this.maxComplexity} that is the max allowed complexity.`
          );
        }
      },
    };
  }
}
