import { Request, Response } from "express";
import { ApolloComplexityPlugin } from "./plugins/ApolloComplexityPlugin";
import { GraphQLError } from "graphql";
import { AuthenticationError } from "apollo-server-errors";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import * as dotenv from 'dotenv';
dotenv.config()
export const gqlOptions: ApolloDriverConfig = {
  driver: ApolloDriver,
  playground: process.env.NODE_ENV !== 'production' ? true : false,
  fieldResolverEnhancers: ["guards", "interceptors"],
  useGlobalPrefix: false,
  autoSchemaFile: true,
  plugins: [new ApolloComplexityPlugin(200)],
  introspection: process.env.NODE_ENV !== 'production' ? true : false,
  // autoTransformHttpErrors: true,
  formatError: (error: GraphQLError) => {
    if (
      error.extensions?.exception?.code === "401" ||
      error.message === "Unauthorized"
    ) {
      return new AuthenticationError("Unauthorized");
    }
    return {
      ...error,
      stack: process.env.NODE_ENV === 'production' ? undefined : error.stack
    }
  },
  context: ({
    req,
    res,
    connection,
  }: {
    req: Request;
    res: Response;
    connection: { context: Request };
  }) => {
    if (connection) {
      // check connection for metadata
      return { req: connection.context, res };
    } else {
      // check from req
      // return new GraphQLContext(req, res);
      return { req, res };
    }
  },
};
