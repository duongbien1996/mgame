import { InterfaceType, Field, ObjectType, Int } from "@nestjs/graphql";

@InterfaceType()
export abstract class Connection {
  @Field(() => Int)
  itemCount: number;

  @Field(() => Int)
  totalItems: number;

  @Field(() => Int)
  pageCount: number;

  next?: string;
  previous?: string;
}

@ObjectType()
export class BasePaginationMeta {
  @Field()
  itemCount: number;
  /**
   * the total amount of items
   */
  @Field()
  totalItems: number;
  /**
   * the amount of items that were requested per page
   */
  @Field()
  itemsPerPage: number;
  /**
   * the total amount of pages in this paginator
   */
  @Field()
  totalPages: number;
  /**
   * the current page this paginator "points" to
   */
  @Field()
  currentPage: number;
}

@ObjectType()
export class CreatorInfo {
  @Field({ nullable: true })
  _id?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  avatar?: string;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  objectType?: number;

}

// export function PaginationBase<T>(classRef: Type<T>) {
//   @ObjectType({ isAbstract: true })
//   abstract class PaginatedType {
//     @Field(() => [classRef], { nullable: true })
//     items: T[];

//     @Field(() => BasePaginationMeta)
//     meta: BasePaginationMeta;
//   }
//   return PaginatedType;
// }