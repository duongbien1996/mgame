import { ExecutionContext, Injectable, Scope } from "@nestjs/common";
import { GqlExecutionContext } from "@nestjs/graphql";
import { AuthGuard } from "@nestjs/passport";
import { ThrottlerGuard } from "@nestjs/throttler";
import { Request } from "express";

@Injectable({
  scope: Scope.REQUEST,
})
export class GqlAuthGuard extends AuthGuard("bearer") {
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context).getContext<{
      req: Request;
    }>();
    return ctx.req;
  }
}

@Injectable()
export class GqlThrottlerGuard extends ThrottlerGuard {
  getRequestResponse(context: ExecutionContext) {
    const gqlCtx = GqlExecutionContext.create(context);
    const ctx = gqlCtx.getContext();
    return { req: ctx.req, res: ctx.res };
  }
}
@Injectable()
export class BasicAuthGuard extends AuthGuard('basic') {
  getRequest(context: ExecutionContext) {

    return GqlExecutionContext.create(context).getContext().req;

  }
}