/// <reference types="node" />

declare namespace NodeJS {
    interface ProcessEnv {
      readonly PORT: string;
      readonly NODE_ENV?: "production" | "development";
  
      readonly MONGODB_URL: string;
      readonly DB_PASS?: string;
      readonly DB_NAME?: string;
      readonly DB_USER?: string;
  
      //Kafka
      readonly KAFKA_HOST: string;
      readonly KAFKA_PORT: string;
      readonly KAFKA_PREFIX: string;
  
      // Queue;
      readonly REDIS_HOST: string;
      readonly REDIS_PORT: string;
      readonly REDIS_PASSWORD?: string;
  
      //es
      readonly ELS_HOST: string;
      readonly ELS_PORT: string;
  
    }
  }
  