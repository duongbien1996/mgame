import { Prop } from "@nestjs/mongoose";
import { Schema as SchemaType } from "mongoose";
import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType({ isAbstract: true })
export class BaseModel {
  @Field(() => String)
  _id: SchemaType.Types.ObjectId;

  @Field()
  @Prop({ type: Date })
  createdAt: Date;

  @Field()
  @Prop({ type: Date })
  updatedAt: Date;
}
