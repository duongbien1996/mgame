import mongoose, {
    Document,
    FilterQuery,
    Model,
    QueryOptions,
    Types,
    UpdateQuery,
  } from "mongoose";
  import { BasePaginationArgs } from "src/graphql/types/common.args";
  import { createPaginationObject } from "src/helpers/resolve-pagination";
  import { DeepPartial } from "typeorm/common/DeepPartial";
  
  export abstract class EntityRepository<T extends Document> {
    constructor(protected readonly entityModel: Model<T>) { }
  
    estimatedDocumentCount() {
      return this.entityModel.collection.estimatedDocumentCount();
    }
  
    async paginate(
      paginationOptions: BasePaginationArgs,
      filters?: FilterQuery<T>,
      options?: QueryOptions<T> | null | undefined
    ) {
      const { page, limit } = paginationOptions;
      const [items, total] = await Promise.all([
        this.entityModel.find(filters, null, {
          skip: (page - 1) * limit,
          limit,
          ...options,
        }).lean(),
        this.entityModel.count(filters),
      ]);
  
      return createPaginationObject(items, total, page, limit);
    }
  
    async findOne(
      entityFilterQuery: FilterQuery<T>,
      projection?: Record<string, unknown>
    ): Promise<T | null> {
      return this.entityModel
        .findOne(entityFilterQuery, {
          // _id: 0,
          __v: 0,
          ...projection,
        })
        .exec();
    }
  
    async find(
      entityFilterQuery: FilterQuery<T>,
      options?: QueryOptions<T> | null | undefined
    ): Promise<T[] | null> {
      return await this.entityModel.find(entityFilterQuery, null, options).lean();
    }
  
    async findByIds(
      ids: ReadonlyArray<string>,
      filters?: FilterQuery<T>
    ): Promise<T[]> {
      return await this.entityModel.find({
        _id: { $in: ids.map((x) => new mongoose.Types.ObjectId(x)) },
        ...filters,
      });
    }
  
    async create(createEntityData: unknown): Promise<T> {
      const entity: T = new this.entityModel(createEntityData);
      return entity.save();
    }
  
    async insertMany(dataMany: ReadonlyArray<object>) {
      return await this.entityModel.insertMany(dataMany)
    }
  
    async findOneAndUpdate(
      entityFilterQuery: FilterQuery<T>,
      updateEntityData: UpdateQuery<unknown>
    ): Promise<T | null> {
      return this.entityModel.findOneAndUpdate(
        entityFilterQuery,
        updateEntityData,
        {
          new: true,
        }
      );
    }
  
    async findOneAndUpdateWithUpsert(
      filter: FilterQuery<T>,
      update: UpdateQuery<T>,
      options?: QueryOptions<T> | null | undefined
    ): Promise<T> {
      return this.entityModel.findOneAndUpdate(filter, update, { upsert: true, ...options }).exec();
    }
  
    async delete(_id: string) {
      try {
        await this.entityModel.findByIdAndUpdate(_id, {
          isDeleted: true,
        });
        return true;
      } catch (error) {
        throw error;
      }
    }
  
    async deleteOne(_id: string) {
      try {
        return await this.entityModel.deleteOne({ _id });
      } catch (error) {
        throw error;
      }
    }
  
    async deleteMany(entityFilterQuery: FilterQuery<T>): Promise<boolean> {
      const deleteResult = await this.entityModel.deleteMany(entityFilterQuery);
      return deleteResult.deletedCount >= 1;
    }
  
    async countDocuments(entityFilterQuery: FilterQuery<T>): Promise<number> {
      return await this.entityModel.countDocuments(entityFilterQuery).lean();
    }
  
  }
  