import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import {
  MongooseModuleOptions,
  MongooseOptionsFactory,
} from "@nestjs/mongoose";

@Injectable()
export class MongooseConfigService implements MongooseOptionsFactory {
  constructor(private readonly configService: ConfigService) { }

  createMongooseOptions(): MongooseModuleOptions {
    return {
      uri: this.configService.get("database.host"),
      // pass: this.configService.get("database.pass"),
      // user: this.configService.get("database.user"),
      // dbName: this.configService.get("database.name"),
      autoIndex: false,
      maxPoolSize: 10,
      serverSelectionTimeoutMS: 5000,
      // useUnifiedTopology: true,
      // useNewUrlParser: true,
      socketTimeoutMS: 45000,
      family: 4 as 4 | 6 | undefined,
      retryAttempts: 3,
      connectionFactory: (connection) => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        connection.plugin(require("mongoose-paginate-v2"));
        return connection;
      },
    };
  }
}
