import { Global, Module } from "@nestjs/common";
import { AuthService } from "src/modules/auth/services/auth.service";
import { JwtStrategy } from "src/modules/auth/strategies/jwt.strategy";
import { HttpModule } from "@nestjs/axios";
import { PassportModule } from "@nestjs/passport";
import { ConfigModule } from "@nestjs/config";
import { BasicStrategy } from "./auth-basic.strategy";

@Global()
@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
    }),
    PassportModule,
    ConfigModule
  ],
  providers: [JwtStrategy, AuthService, BasicStrategy],
})
export class AuthModule { }
