import { Injectable } from "@nestjs/common";
import { HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
import { catchError, firstValueFrom } from "rxjs";

@Injectable()
export class AuthService {
  constructor(
    private readonly httpService: HttpService,
    private configService: ConfigService
  ) { }

  getMeeyIdData = async (meeyIdAccessToken: string) => {
    if (!meeyIdAccessToken) {
      return null;
    }

    const headers: any = {
      "X-CLIENT-ID": this.configService.get("meeyId.clientId"),
      Authorization: `Bearer ${meeyIdAccessToken}`,
      "Content-Type": "application/json",
      "Accept-Encoding": "application/json",
    };
    const { data }: any = await firstValueFrom(
      this.httpService
        .get(
          this.configService.get("meeyId.api") +
          this.configService.get("meeyId.path"),
          {
            headers,
            timeout: 1000 * 10
          }
        )
        .pipe(
          catchError((err) => {
            throw err;
          })
        )
    );
    if (!data.error.status) {
      return null;
    }
    return data.data;
    // return {
    //   id: "5e7676d05071cd1c9e8f6d83",
    // };
  };
}
