import { Strategy } from "passport-http-bearer";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { Request } from "express";
import { AuthService } from "src/modules/auth/services/auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: (req: Request) => {
        return (
          (req.cookies.token as string) ??
          req.headers.authorization?.split(" ")[1] ??
          req.query.access_token
        );
      },
      ignoreExpiration: true,
      passReqToCallback: true,
    });
  }

  validate = async (req: Request) => {
    
  };
}
