import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { EntityRepository } from "src/database/repository/base.repository";
import { Category, CategoryDocument } from "src/modules/categories/models/category.model";


@Injectable()
export class CategoryRepository extends EntityRepository<CategoryDocument> {
  constructor(@InjectModel(Category.name) private model: Model<CategoryDocument>) {
    super(model);
  }
}