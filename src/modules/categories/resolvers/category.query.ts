import { Query, Resolver, Args } from "@nestjs/graphql";
import { Category } from "src/modules/categories/models/category.model";
import { CategoryService } from "src/modules/categories/services/category.service";

@Resolver(() => Category)
export class CategoryQueryResolver {
  constructor(
    private readonly categoryService: CategoryService,
  ) { }

  @Query(() => Category)
  category(@Args("id") id: string) {
    console.log(222)
  }
}
