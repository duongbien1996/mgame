import { Category } from "src/modules/categories/models/category.model";
import { Resolver, Mutation, Args } from "@nestjs/graphql";
import {
  CreateCategoryDto,
  UpdateCategoryDto,
} from "src/modules/categories/dtos/category.input";
import { CategoryService } from "src/modules/categories/services/category.service";
import { Authenticated, CurrentUser } from "src/decorators/common.decorator";

@Authenticated()
@Resolver(() => Category)
export class CategoryMutationResolver {
  constructor(private readonly categoryService: CategoryService) { }

  @Mutation(() => Category)
  createCategory(@Args("input") input: CreateCategoryDto, @CurrentUser() user) {
    return this.categoryService.create();
  }
}
