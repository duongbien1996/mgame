import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { Category } from "src/modules/categories/models/category.model";
import { CategoryService } from "src/modules/categories/services/category.service";

@Resolver(() => Category)
export class CategoryFieldResolver {
  constructor(
    private readonly categoryService: CategoryService,
  ) {}

 
}
