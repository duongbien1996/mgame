import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { ObjectType } from "@nestjs/graphql";
import { HydratedDocument, Schema as SchemaType } from "mongoose";
import { BasePaginationMeta } from "src/graphql/types/common.interface.entity";
import { Field } from "@nestjs/graphql";
import { BaseModel } from "src/database/base.model";
import { DeepPartial } from "typeorm";

export type CategoryDocument = HydratedDocument<Category>;

@ObjectType()
@Schema({
  timestamps: true,
  id: false,
  _id: true,
})
export class Category extends BaseModel {
  @Field()
  @Prop({ required: true })
  name: string;

  @Field({ nullable: true })
  @Prop()
  description: string;

  @Field(() => String, { nullable: true })
  @Prop()
  parentCategoryId?: SchemaType.Types.ObjectId;

  @Field({ nullable: true })
  @Prop({ default: 0 })
  hierarchy?: number;

  @Field({ nullable: true })
  @Prop()
  slug?: string;

  @Field({ defaultValue: 0, nullable: true })
  @Prop()
  count?: number;

  @Field(() => String, { nullable: true })
  @Prop()
  createdBy?: SchemaType.Types.ObjectId;

  @Field(() => String, { nullable: true })
  @Prop()
  updatedBy?: SchemaType.Types.ObjectId;

  constructor(partial: DeepPartial<Category>) {
    super();
    Object.assign(this, { ...partial });
  }
}

@ObjectType()
export class CategoryConnection {
  @Field(() => [Category], { nullable: true })
  items: Category[];

  @Field(() => BasePaginationMeta)
  meta: BasePaginationMeta;
}

export const CategorySchema = SchemaFactory.createForClass(Category);
