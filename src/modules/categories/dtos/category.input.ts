import { InputType, Field, PartialType } from "@nestjs/graphql";
import { MaxLength } from "class-validator";

@InputType()
export class CreateCategoryDto {
  @MaxLength(50, { message: "tên tối đa 50 kí tự" })
  @Field()
  name: string;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  parentCategoryId?: string;

  @Field({ defaultValue: 0 })
  hierarchy?: number;

  @Field({ nullable: true })
  slug?: string;

  @Field({ nullable: true })
  createdBy?: string;
}
@InputType()
export class UpdateCategoryDto extends PartialType(CreateCategoryDto) {
  @Field()
  _id: string;

  @Field({ nullable: true })
  updatedBy?: string;
}
