import { ArgsType, Field } from "@nestjs/graphql";
import { BasePaginationArgs } from "src/graphql/types/common.args";

@ArgsType()
export class CategoryPaginateDto extends BasePaginationArgs {
  @Field({ nullable: true })
  search?: string;
}
