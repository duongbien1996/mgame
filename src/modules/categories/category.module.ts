import { forwardRef, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import {
  Category,
  CategorySchema,
} from "src/modules/categories/models/category.model";
import { CategoryRepository } from "src/modules/categories/repositories/category.repository";
import { CategoryMutationResolver } from "src/modules/categories/resolvers/category.mutation";
import { CategoryQueryResolver } from "src/modules/categories/resolvers/category.query";
import { CategoryService } from "src/modules/categories/services/category.service";
import { HttpModule } from "@nestjs/axios";
import { CategoryFieldResolver } from "src/modules/categories/resolvers/category.field";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Category.name,
        schema: CategorySchema,
      },
    ]),
    HttpModule.register({
      timeout: 10000,
    }),
  ],
  providers: [
    CategoryFieldResolver,
    CategoryQueryResolver,
    CategoryMutationResolver,
    CategoryService,
    CategoryRepository,
    CategoryService,
  ],
  exports: [CategoryService],
})
export class CategoryModule {}
