export default () => ({
    port: parseInt(process.env.PORT, 10) || 3000,
    database: {
      host: process.env.MONGODB_URL,
      user: process.env.DB_USER,
      pass: process.env.DB_PASS,
      name: process.env.DB_NAME,
    },
    redis: {
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASSWORD,
      url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
    },
    elasticsearch: {
      node: process.env.ELS_HOST,
      user: process.env.ELS_USER,
      password: process.env.ELS_PASS
      // node: `${process.env.ELS_HOST}:${process.env.ELS_PORT}`,
    },
    kafka: {
      url: `${process.env.KAFKA_HOST}`,
      prefix: process.env.KAFKA_PREFIX,
    },
  });
  